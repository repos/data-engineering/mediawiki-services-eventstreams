// TODO: Fix eslint strict mode.
// https://eslint.org/docs/latest/rules/strict
// > In the CommonJS module system, a hidden function wraps each module and limits the scope of a “global” strict mode directive.
// > In ECMAScript modules, which always have strict mode semantics, the directives are unnecessary.

const { ServiceUtils } = require('@wikimedia/service-utils');
const startApp = require('../../app.js');

class TestExpressServer {
    configDirectory;
    running = false;
    conf;
    app;
    spec;
    uri;

    constructor(configDirectory = `${__dirname}/../../`) {
        this.configDirectory = configDirectory;
    }

    async start() {
        if (this.running) {
            return;
        }
        this.conf = (await ServiceUtils
            .loadConfig({
                cwd: this.configDirectory,
                envName: 'test',
                configFile: 'config',
                overrides: { logging: { level: 'debug', format: 'simple' } }
            }))
            .services[0].conf;
        this.app = await startApp({
            configDirectory: this.configDirectory,
            envName: 'test',
            // Don't start metrics endpoint just in case service is running when tests are running
            startPrometheusServer: false
        });
        this.uri = `http://${this.conf.interface ?? '127.0.0.1'}:${this.conf.port}/`;
        this.spec = await fetch(`${this.uri}?spec`)
            .then((res) => {
                if (!res.body) { // TODO: can use res.json() if the tests are converted
                    throw new Error('Failed to get spec');
                }
                return res.body;
            })
            .catch((err) => {
                // this error will be detected later, so ignore it
                this.spec = { paths: {}, 'x-default-params': {} };
            });
        this.running = true;
    }

    get config() {
        if (!this.running) {
            throw new Error('Accessing test service config before starting the service');
        }
        return {
            uri: this.uri,
            conf: this.conf,
            spec: this.spec,
        };
    }

    async stop() {
        await this.app.close();
    }
}

module.exports = TestExpressServer;
