/* global describe, it, before, after */

'use strict';

const assert = require('../../utils/assert.js');
const Server = require('../../utils/server.js');

describe('service information', function () {

    this.timeout(20000);

    let infoUri = null;

    const server = new Server();

    before(() => {
        return server.start()
        .then(() => {
            infoUri = `${server.config.uri}_info/`;
        });
    });

    after(() => server.stop());

    // common function used for generating requests
    // and checking their return values
    async function checkRet(fieldName) {
        const res = await fetch(infoUri + fieldName);
        // check the returned Content-Type header
        assert.contentType(res, 'application/json');
        const body = await res.json();
        // the status as well
        assert.status(res, 200);
        // finally, check the body has the specified field
        assert.notDeepEqual(body, undefined, 'No body returned!');
        assert.notDeepEqual(body[fieldName], undefined, `No ${fieldName} field returned!`);
    }

    it('should get the service name', () => {
        return checkRet('name');
    });

    it('should get the service version', () => {
        return checkRet('version');
    });

    it('should redirect to the service home page', async () => {
        const res = await fetch(`${infoUri}home`, { redirect: 'manual' });
        assert.status(res, 301);
    });

    it('should get the service info', async () => {
        const res = await fetch(infoUri);
        // check the status
        assert.status(res, 200);
        // check the returned Content-Type header
        assert.contentType(res, 'application/json');
        const body = await res.json();
        // inspect the body
        assert.notDeepEqual(body, undefined, 'No body returned!');
        assert.notDeepEqual(body.name, undefined, 'No name field returned!');
        assert.notDeepEqual(body.version, undefined, 'No version field returned!');
        assert.notDeepEqual(body.description, undefined, 'No description field returned!');
        assert.notDeepEqual(body.home, undefined, 'No home field returned!');
    });
});
