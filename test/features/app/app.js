const assert = require('../../utils/assert.js');
const Server = require('../../utils/server.js');

describe('express app', function () {

    this.timeout(20000);

    const server = new Server();

    before(() => server.start());

    after(() => server.stop());

    it('should get robots.txt', async () => {
        const res = await fetch(`${server.config.uri}robots.txt`);
        assert.deepEqual(res.status, 200);
        assert.deepEqual(await res.text(), 'User-agent: *\nDisallow: /\n');
    });

    it('should set CORS headers', async () => {
        if (server.config.conf.cors === false) {
            return true;
        }
        const res = await fetch(`${server.config.uri}robots.txt`);
        assert.deepEqual(res.status, 200);
        assert.deepEqual(res.headers.get('access-control-allow-origin'), '*');
        assert.deepEqual(!!res.headers.get('access-control-allow-headers'), true);
        assert.deepEqual(!!res.headers.get('access-control-expose-headers'), true);
    });

    it('should set CSP headers', async () => {
        if (server.config.conf.csp === false) {
            return true;
        }
        const res = await fetch(`${server.config.uri}robots.txt`);
        assert.deepEqual(res.status, 200);
        assert.deepEqual(res.headers.get('x-xss-protection'), '1; mode=block');
        assert.deepEqual(res.headers.get('x-content-type-options'), 'nosniff');
        assert.deepEqual(res.headers.get('x-frame-options'), 'SAMEORIGIN');
        assert.deepEqual(res.headers.get('content-security-policy'), 'default-src \'self\'; object-src \'none\'; media-src *; img-src *; style-src *; frame-ancestors \'self\'');
        assert.deepEqual(res.headers.get('x-content-security-policy'), 'default-src \'self\'; object-src \'none\'; media-src *; img-src *; style-src *; frame-ancestors \'self\'');
        assert.deepEqual(res.headers.get('x-webkit-csp'), 'default-src \'self\'; object-src \'none\'; media-src *; img-src *; style-src *; frame-ancestors \'self\'');
    });

// === EventStreams modification ===
    it.skip('should get static content gzipped', async () => {
// === End EventStreams modification ===
        const res = await fetch(`${server.config.uri}static/index.html`, { headers: { 'accept-encoding': 'gzip, deflate' } });
        assert.deepEqual(res.status, 200);
        // if there is no content-length, the reponse was gzipped
        assert.deepEqual(res.headers.get('content-length'), undefined,
            'Did not expect the content-length header!');
    });

// === EventStreams modification ===
    it.skip('should get static content uncompressed', async () => {
// === End EventStreams modification ===
        const res = await fetch(`${server.config.uri}static/index.html`, { headers: { 'accept-encoding': '' } });
        const contentEncoding = res.headers.get('content-encoding');
        assert.deepEqual(res.status, 200);
        assert.deepEqual(contentEncoding, undefined, 'Did not expect gzipped contents!');
    });
});
