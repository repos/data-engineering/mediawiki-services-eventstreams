'use strict';

const _ = require('lodash');
const assert = require('../utils/assert.js');
const { makeMediaWikiRedactorDeserializer, deserializer } = require('../../lib/eventstreams-util');

/**
 * Creates a helper function to make kafka messages with default values
 *
 * @param {Object} value_defaults
 * @return {function(Object): {value: string}}
 */
function createKafkaMessageWithDefaults(value_defaults) {

    return (value_overrides = {}, key = null) => {
        return {
            // KafkaSSE will return a string in a Buffer, value is always JSON.
            value: Buffer.from(JSON.stringify(_.merge(value_defaults, value_overrides))),
            topic: '',
            partition: 0,
            offset: 0,
            timestamp: null,
            // key is likely JSON, but we'll let the producer decide.
            // deserializer is smart enough to figure out how to deserialize to JSON if needed.
            key,
        }
    }
};



describe('eventstream-util', () => {
    let createKafkaMessage;
    before(() => {
        createKafkaMessage = createKafkaMessageWithDefaults({
            meta: {
                stream: 'mediawiki.page_change.v1',
                domain: 'test.domain'
            },
            page: {
                page_title: 'redact'
            },
            performer: {
                user_id: 123,
                user_text: 'example'
            },
            revision: {
                editor: {
                    user_id: 123,
                    user_text: 'example'
                }
            },
            prior_state: {
                revision: {
                    editor: {
                        user_id: 123,
                        user_text: 'example'
                    }
                },
            },
        });
    });

    describe('deserializer', () => {
        context('key deserialization', () => {

            /**
             * Helper function to DRY assertions for testing kafka key deserialization.
             * The key will be serialized using serializeKey, and then
             * that value will be passed through eventstream-util deserializer.
             * key will be asserted to be deep equal to the deserialized key.
             * @param {*} key Key to test
             * @param {*} serializeKey function that serializes key
             */
            function assertKeyDeserialization(key, serializeKey = k => k) {
                const kafkaMessage = createKafkaMessage({}, serializeKey(key));
                const deserializedKafkaMessage = deserializer(kafkaMessage);
                assert.deepEqual(deserializedKafkaMessage.message.meta.key, key);
            }

            const testCases = [
                {
                    title: 'should deserialize integer kafka key',
                    key: 123,
                    serializeKey: (k) => k
                },
                {
                    title: 'should deserialize string kafka key',
                    key: "abc",
                    serializeKey: (k) => Buffer.from(k, 'utf-8')
                },
                {
                    title: 'should deserialize JSON object kafka key',
                    key: {
                        wiki_id: "abc",
                        page_id: 123
                    },
                    serializeKey: (k) => Buffer.from(JSON.stringify(k), 'utf-8')
                },
                {
                    title: 'should deserialize JSON string kafka key',
                    key: '"abc"',
                    serializeKey: (k) => Buffer.from(JSON.stringify(k), 'utf-8')
                },
            ];

            testCases.forEach(({ title, key, serializeKey }) => {
                it(title, () => {
                    assertKeyDeserialization(key, serializeKey);
                });
            });

        });
    });

    context('makeMediaWikiRedactorDeserializer', () => {
        context('mediawiki.page_change.v1', () => {
            const assertRedactedPageChange = (redacted_page) => {
                // Performer is required by the schema but its properties aren't.
                assert.ok(redacted_page.performer);
                assert.ok(!redacted_page.performer?.user_id);
                assert.ok(!redacted_page.performer?.user_text);

                assert.ok(!redacted_page.revision?.editor);
                assert.ok(!redacted_page.prior_state.revision?.editor);
            };

            it('should redact mediawiki.page_change.v1 message correctly', () => {
                const redactPage1 = createKafkaMessage();
                const redactPage2 = createKafkaMessage({
                    page: { page_title: 'redact_this' }
                });

                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['redact', 'Redact this'] });
                const { message: redactedPage1 } = redactor(redactPage1);
                const { message: redactedPage2 } = redactor(redactPage2);
                assertRedactedPageChange(redactedPage1);
                assertRedactedPageChange(redactedPage2);
            });

            it('should not redact mediawiki.page_change.v1 message correctly', () => {
                const redactPage = createKafkaMessage({
                    meta: { domain: 'other.domain' },
                    page: {
                        page_title: 'no redact'
                    },
                });

                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['no redact'] });
                const { message: redactedPage } = redactor(redactPage);

                assert.ok(redactedPage.performer.user_id);
                assert.ok(redactedPage.performer.user_text);

                assert.ok(redactedPage.revision.editor);
                assert.ok(redactedPage.prior_state.revision.editor);
            });
        });

        context('mediawiki.recentchange', () => {
            let createKafkaMessage;
            before(() => {
                createKafkaMessage = createKafkaMessageWithDefaults({
                    meta: {
                        stream: 'mediawiki.recentchange',
                        domain: 'test.domain'
                    },
                    title: 'redact',
                    user: {
                        user_text: 'example'
                    },
                });
            });

            it('should redact mediawiki.recentchange message correctly', () => {
                const redactPage1 = createKafkaMessage();
                const redactPage2 = createKafkaMessage({ title: 'redact this' });

                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['redact', 'Redact this'] });
                const { message: redactedPage1 } = redactor(redactPage1);
                const { message: redactedPage2 } = redactor(redactPage2);

                assert.ok(!redactedPage1?.user);
                assert.ok(!redactedPage2?.user);
            });

            it('should not redact mediawiki.recentchange message correctly', () => {
                const redactPage = createKafkaMessage({
                    meta: { domain: 'other.domain' },
                    title: 'no redact'
                });

                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['no redact'] });
                const { message: redactedPage } = redactor(redactPage);

                assert.ok(redactedPage.user);
            });
        });

        context('other streams', () => {
            let createKafkaMessage;
            before(() => {
                createKafkaMessage = createKafkaMessageWithDefaults({
                    page_title: 'redact',
                    performer: {
                        user_id: 123,
                        user_text: 'example'
                    },
                });
            });

            it('should redact other performer fragments correctly', () => {
                const redactPage = createKafkaMessage({ meta: { domain: 'test.domain' } });
                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['redact'] });
                const { message: redactedPage } = redactor(redactPage);

                assert.ok(!redactedPage?.performer);
            });

            it('should not redact other performer fragments correctly', () => {
                const redactPage = createKafkaMessage({
                    page_title: 'no redact',
                    meta: { domain: 'other.domain' }
                });
                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['redact this'] });
                const { message: redactedPage } = redactor(redactPage);

                assert.ok(redactedPage.performer);
            });

            it('should redact titles with spaces correctly', () => {
                const redactPage = createKafkaMessage({ meta: { domain: 'test.domain' }, page_title: 'redact_this' });
                const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['redact this'] });
                const { message: redactedPage } = redactor(redactPage);

                assert.ok(!redactedPage?.performer);
            });
        });

        context('edge cases', () => {
            const createKafkaMessage = createKafkaMessageWithDefaults();

            const testCases = [
                {
                    title: 'should not fail on empty message',
                    message: {}
                },
                {
                    title: 'should not fail on no title',
                    message: { meta: { domain: 'test.domain' } }
                },
                {
                    title: 'should parse boolean page title',
                    message: {
                        meta: { domain: 'test.domain', stream: 'mediawiki.recentchange' },
                        title: 'false', user: {}
                    },
                    redacted: true
                },
                {
                    title: 'should parse number page title',
                    message: {
                        meta: { domain: 'test.domain', stream: 'mediawiki.page_change.v1' },
                        page: { page_title: '404' }, revision: { editor: {} }
                    },
                    redacted: true
                },
            ];

            const redactor = makeMediaWikiRedactorDeserializer({ 'test.domain': ['redact', 'Redact this', 404, false] });

            testCases.forEach(({ title, message, redacted }) => {
                it(title, () => {
                    const redactedPage = redactor(createKafkaMessage(message));
                    assert.ok(_.isMatch(redactedPage.message, message) !== !!redacted);
                    assert.ok(_.has(redactedPage, 'topic'));
                    assert.ok(_.has(redactedPage, 'offset'));
                    assert.ok(_.has(redactedPage, 'partition'));
                    assert.ok(_.has(redactedPage, 'key'));
                });
            });
        });
    });
});
